package Model;

import javax.persistence.*;
import javax.validation.constraints.Email;

@Entity
public class User {

    @Id @GeneratedValue
    private int id;

    @Email(message = "Email should be valid")
    private String UserName;

    public User() {
    }

    public User(@Email(message = "Email should be valid") String userName) {
        UserName = userName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }
}
