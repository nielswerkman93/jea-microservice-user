package API.Service;

import Model.User;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class DefaultUserService implements UserService{

    @PersistenceContext(unitName = "PU")
    private EntityManager entityManager;


    @Override
    public User getById(int id) {
        return entityManager.createQuery("SELECT u FROM User u WHERE u.id=:id", User.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public List<User> getAll() {
        return entityManager.createQuery("SELECT u FROM User u", User.class).getResultList();
    }

    @Override
    @Transactional
    public void delete(User user) {
        try{
            entityManager.remove(entityManager.contains(user) ? user : entityManager.merge(user));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public User add(User user) {
        try{
            entityManager.persist(user);
        }catch (Exception e){
            e.printStackTrace();
        }
        return user;
    }

    @Override
    @Transactional
    public User getUserByEmail(String email) {
        User user = null;
        try{
            user = entityManager.createQuery("SELECT u FROM User u WHERE u.UserName=:username", User.class)
                    .setParameter("username", email)
                    .getSingleResult();
        }catch (NoResultException ex){
            ex.printStackTrace();
        }
        return user;
    }

    @Override
    public User update(User user) {
        try{
            entityManager.merge(user);
        }catch (Exception e){
            e.printStackTrace();
        }
        return user;
    }
}
