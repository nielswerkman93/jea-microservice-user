package API.Service;

import Model.User;

import java.util.List;

public interface UserService {
    User getById(int id);
    List<User> getAll();
    void delete(User user);
    User add(User user);
    User getUserByEmail(String email);
    User update(User user);
}
