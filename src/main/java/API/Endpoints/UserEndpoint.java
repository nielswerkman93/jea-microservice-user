package API.Endpoints;

import API.Service.UserService;
import Model.User;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Produces(MediaType.APPLICATION_JSON)
@Path("/user")
public class UserEndpoint {

    @Inject
    private UserService service;

    @Context
    UriInfo uriInfo;

    @GET
    @Path("/{id}")
    public Response getUserById(@PathParam("id") int id){
        User user;

        try{
            user = service.getById(id);
        } catch (Exception e){
            return Response.status(Response.Status.BAD_REQUEST.getStatusCode(), e.toString()).build();
        }

        return Response.ok()
                .entity(user)
                .link(uriInfo.getAbsolutePathBuilder().build(id), "self")
                .build();
    }

    @GET
    @Path("/{email}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getUserByEmail(@PathParam("email") String email){
        User user;

        try{
            user = service.getUserByEmail(email);
        } catch (Exception e){
            return Response.status(Response.Status.BAD_REQUEST.getStatusCode(), e.toString()).build();
        }

        if(user == null){
            return Response.status(Response.Status.NOT_FOUND.getStatusCode()).build();
        }

        return Response.ok()
                .entity(user)
                .link(uriInfo.getAbsolutePathBuilder().build(email), "self")
                .build();
    }

    @GET
    public Response getAll(){
        List<User> users;

        try{
            users = service.getAll();
        } catch (Exception e){
            return Response.status(Response.Status.BAD_REQUEST.getStatusCode(), e.toString()).build();
        }
        return Response.ok()
                .entity(users)
                .link(uriInfo.getAbsolutePathBuilder().build(), "self")
                .link(uriInfo.getBaseUriBuilder().path("/api/bericht").build(), "berichten")
                .build();
    }

    @POST
    public Response add(@Valid User user){
        try{
            service.add(user);
        } catch (Exception e){
            return Response.status(Response.Status.BAD_REQUEST.getStatusCode(), e.toString()).build();
        }
        return Response.status(Response.Status.CREATED.getStatusCode()).build();
    }

    @PUT
    public Response update(@Valid User user){
        try{
            service.update(user);
        } catch (Exception e){
            return Response.status(Response.Status.BAD_REQUEST.getStatusCode(), e.toString()).build();
        }
        return Response.status(Response.Status.OK.getStatusCode()).build();
    }
}
